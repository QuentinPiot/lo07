<?php
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$compteCree = false;
if (isset($_POST['forminscription'])) {
  $pseudo = htmlspecialchars($_POST['pseudo']);
  $mail = htmlspecialchars($_POST['mail']);
  $mail2 = htmlspecialchars($_POST['mail2']);
  $mdp = sha1($_POST['mdp']);
  $mdp2 = sha1($_POST['mdp2']);
  if (!empty($_POST['pseudo']) and !empty($_POST['mail']) and !empty($_POST['mail2']) and !empty($_POST['mdp']) and !empty($_POST['mdp2'])) {
    $pseudolength = strlen($pseudo);
    if ($pseudolength <= 255) {
      if ($mail == $mail2) {
        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
          $reqmail = $bdd->prepare("SELECT * FROM membres WHERE mail = ?");
          $reqmail->execute(array($mail));
          $mailexist = $reqmail->rowCount();
          if ($mailexist == 0) {
            if ($mdp == $mdp2) {
              $requete = "INSERT INTO membres(pseudo, mail, motdepasse, admin) VALUES('$pseudo', '$mail', '$mdp',0)";
              $insertmbr = $bdd->prepare($requete);
              $insertmbr->execute();
              $compteCree = true;
            } else {
              $erreurMessage = "Vos mots de passes ne correspondent pas !";
            }
          } else {
            $erreurMessage = "Adresse mail déjà utilisée !";
          }
        } else {
          $erreurMessage = "Votre adresse mail n'est pas valide !";
        }
      } else {
        $erreurMessage = "Vos adresses mail ne correspondent pas !";
      }
    } else {
      $erreurMessage = "Votre pseudo ne doit pas dépasser 255 caractères !";
    }
  } else {
    $erreurMessage = "Tous les champs doivent être complétés !";
  }
}
?>
<html>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Inscription</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Inscription</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li class="active">Nous contacter</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="span12">
          <?php
          if (isset($erreurMessage)) {
            echo '<p class="text-error" style= "font-size : 20px">' . $erreurMessage . "</p>";
          }
          ?>
          <br><br>
          <?php if (!$compteCree) { ?>
            <form method="POST" action="">
              <div class="form-group">
                <label for="exampleInputEmail1">Pseudonyme :</label>
                <input type="text" placeholder="Votre pseudo" id="pseudo" name="pseudo" style="height:30px;font-size:10pt;" value="<?php if (isset($pseudo)) {
                                                                                                                                      echo $pseudo;
                                                                                                                                    } ?>" />
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputEmail1">Adresse email :</label>
                <input type="email" placeholder="Email" id="mail" name="mail" style="height:30px;font-size:10pt;" value="<?php if (isset($mail)) {
                                                                                                                            echo $mail;
                                                                                                                          } ?>" />
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputEmail1">Confirmation de vore adresse email :</label>
                <input type="email" placeholder="Email" id="mail2" name="mail2" style="height:30px;font-size:10pt;" value="<?php if (isset($mail2)) {
                                                                                                                              echo $mail2;
                                                                                                                            } ?>" />
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputEmail1">Mot de Passe :</label>
                <input type="password" placeholder="Mot de passe" id="mdp" name="mdp" style="height:30px;font-size:10pt;" />
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputEmail1">Confirmation de votre mot de passe :</label>
                <input type="password" placeholder="Mot de passe" id="mdp2" name="mdp2" style="height:30px;font-size:10pt;" />
              </div>
              <br>
              <button type="submit" class="btn btn-primary" name="forminscription" />S'inscrire</button>
            </form>
          <?php } else {
          echo 'Votre compte a bien été créé, vous allez être redirigé vers le menu principal<meta http-equiv="refresh" content="5;url=index.php" />';
        } ?>
        </div>
      </div>
    </div>
    </div>
    <?php
    include 'footer.php';
    ?>
    </div>
    <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
    <?php
    include 'dependances.php';
    ?>
    <script src="js/custom.js"></script>
</body>
</html>