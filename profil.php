<?php
session_start();
require_once('date.php');
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$aeroportsNoms = array('Biarritz', 'Orly', 'Charles De Gaulle', 'Bordeaux', 'Toulouse', 'Malaga', 'Séoul', 'Tokyo', 'Rio de Janeiro');
if (isset($_POST['idDeleteReservation'])) {
  $idReservation = $_POST['idDeleteReservation'];
  $idVoiture = $_POST['idVoitureDeleteReservation'];
  $dateDebut = $_POST['dateDebutDeleteReservation'];
  $dateFin = $_POST['dateFinDeleteReservation'];
  if(isset($_POST['prixReservation'])){
    $idClient = $_SESSION['id'];
    $prixReservation = $_POST['prixReservation'];
    $nouvelArgent = $_SESSION['argent'] + round(($prixReservation/2), 0, PHP_ROUND_HALF_DOWN);
    $requete_prepareeArgent = $bdd->prepare("UPDATE membres SET argent = '$nouvelArgent' WHERE id= $idClient");
    $requete_prepareeArgent->execute();
    $_SESSION['argent'] = $nouvelArgent;
  }else{
    $requeteClient = "SELECT client FROM reservation WHERE numero LIKE $idReservation";
    $requete_prepareeClient = $bdd->prepare($requeteClient);
    $requete_prepareeClient->execute();
    if (!$requete_prepareeClient->rowCount() == 0) {
      while ($results = $requete_prepareeClient->fetch()) {
      
        $idClient = $results[0];
      }
    }
  }
  
 
 
 

  $requete_prepareeDelete = $bdd->prepare("DELETE FROM reservation WHERE numero LIKE $idReservation");
  $requete_prepareeDelete->execute();
  $requete_prepareeDelete2 = $bdd->prepare("SELECT reservation FROM voiture WHERE id LIKE $idVoiture");
  $requete_prepareeDelete2->execute();
  while ($resultsDelete2 = $requete_prepareeDelete2->fetch()) {
    $voitureChoisie = $resultsDelete2[0];
  }
  
  $tableauDates = (array) json_decode($voitureChoisie);

  $index = 0;
  foreach ($tableauDates as $date) {
    if ($date[2] == $idClient && $date[0] == $dateDebut  && $date[1] == $dateFin) {
      unset($tableauDates[$index]);
    }
    $index++;
  }
  if (sizeof($tableauDates) == 0) {
    $nouvelleReservation = "{}";
  } else {
    $nouvelleReservation = json_encode($tableauDates);
  }

  $requeteUpdateReservation = "UPDATE voiture SET reservation = '$nouvelleReservation' WHERE id= $idVoiture";
  $requete_prepareeUpdateReservation = $bdd->prepare($requeteUpdateReservation);
  $requete_prepareeUpdateReservation->execute();
}
if (isset($_GET['id']) and $_GET['id'] > 0) {
  $getid = intval($_GET['id']);
  $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
  $requser->execute(array($getid));
  $userinfo = $requser->fetch();
  $getAdmin = $userinfo['admin'];
  $tableau = array();
  $requete2 = "SELECT * FROM reservation WHERE client LIKE $getid";
  $requete_preparee2 = $bdd->prepare($requete2);
  $requete_preparee2->execute();
  if (!$requete_preparee2->rowCount() == 0) {
    while ($results = $requete_preparee2->fetch()) {
      array_push($tableau, $results);
    }
  } else {
    //echo 'Nothing found';
  };
  $tableauReservaztionAdmin = array();
  $requeteAdmin = "SELECT * FROM reservation";
  $requete_preparee_admin = $bdd->prepare($requeteAdmin);
  $requete_preparee_admin->execute();
  if (!$requete_preparee_admin->rowCount() == 0) {
    while ($results = $requete_preparee_admin->fetch()) {
      $dateFin =  new ObjetDate($results['dateFin']);
      $dateFin = $dateFin->changerFormat();
      $dateAujourdhui = date('Y-m-d', time());
      $dateAujourdhui = new DateTime($dateAujourdhui);
      $interval = $dateFin->diff($dateAujourdhui);
      $interval = $interval->format('%R%a');
      
      if ($interval < 1)       array_push($tableauReservaztionAdmin, $results);
    }
  }
  $tableauLocation = array();
  $requeteLocation = "SELECT * FROM location WHERE client LIKE $getid";
  $requete_prepareeLocation = $bdd->prepare($requeteLocation);
  $requete_prepareeLocation->execute();
  if (!$requete_prepareeLocation->rowCount() == 0) {
    while ($resultsLocation = $requete_prepareeLocation->fetch()) {
      array_push($tableauLocation, $resultsLocation);
    }
  } else {
    //echo 'Nothing found';
  };
  $tableauAdminLocation = array();
  $requeteAdminLocation = "SELECT * FROM location";
  $requete_prepareeAdminLocation = $bdd->prepare($requeteAdminLocation);
  $requete_prepareeAdminLocation->execute();
  if (!$requete_prepareeAdminLocation->rowCount() == 0) {
    while ($resultsLocation = $requete_prepareeAdminLocation->fetch()) {
      $dateFin =  new ObjetDate($resultsLocation['dateFin']);
      $dateFin = $dateFin->changerFormat();
      $dateAujourdhui = date('Y-m-d', time());
      $dateAujourdhui = new DateTime($dateAujourdhui);
      $interval = $dateFin->diff($dateAujourdhui);
      $interval = $interval->format('%R%a');
     
      if ($interval < 1)    array_push($tableauAdminLocation, $resultsLocation);
    

    }
  } else {
    //echo 'Nothing found';
  };
  function getSite($site)
  {
    $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
    $requete3 = "SELECT * FROM sites WHERE id LIKE $site";
    $requete_preparee3 = $bdd->prepare($requete3);
    $requete_preparee3->execute();
    if (!$requete_preparee3->rowCount() == 0) {
      while ($results = $requete_preparee3->fetch()) {
        return $results[3] . " " . $results[2];
      }
    } else {
      //echo 'Nothing found';
    };
  }
  function getInformationsAdministrateur()
  {
    $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
    $sql = "SELECT count(*) FROM `membres`";
    $result = $bdd->prepare($sql);
    $result->execute();
    $nombre_utilisateurs = $result->fetchColumn();
    $sql = "SELECT count(*) FROM `voiture`";
    $result = $bdd->prepare($sql);
    $result->execute();
    $nombre_voiture_creation = $result->fetchColumn();
    $sql = "SELECT count(*) FROM `reservation`";
    $result = $bdd->prepare($sql);
    $result->execute();
    $nombre_reservation_creation = $result->fetchColumn();
    $sql = "SELECT count(*) FROM `location`";
    $result = $bdd->prepare($sql);
    $result->execute();
    $nombre_location_creation = $result->fetchColumn();
    $sql = "SELECT * FROM `voiture` GROUP by marque";
    $result = $bdd->prepare($sql);
    $result->execute();
    $marque_plus_representee = $result->fetchColumn();
    return array('nombre_utilisateurs' => $nombre_utilisateurs, 'nombre_voiture_creation' => $nombre_voiture_creation, 'nombre_reservation_creation' => $nombre_reservation_creation, 'nombre_location_creation' => $nombre_location_creation);
  }
  function getVoiture($voiture)
  {
    $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
    $requete4 = "SELECT * FROM voiture WHERE id LIKE $voiture";
    $requete_preparee4 = $bdd->prepare($requete4);
    $requete_preparee4->execute();
    if (!$requete_preparee4->rowCount() == 0) {
      while ($results = $requete_preparee4->fetch()) {
        return $results[3];
      }
    } else {
      //echo 'Nothing found';
    };
  }
  if ($getAdmin == 1) getInformationsAdministrateur();
  ?>
  <!DOCTYPE html>
  <html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Profil</title>
    <?php include 'classicHead.php' ?>
  </head>
  <body>
    <?php
    include 'header.php';
    ?>
    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span4">
            <div class="inner-heading">
              <h2>Profil de <?php echo $userinfo['pseudo']; ?></h2>
            </div>
          </div>
          <div class="span8">
            <ul class="breadcrumb">
              <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
              <li><a href="#">Pages</a><i class="icon-angle-right"></i></li>
              <li class="active">Profil</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <div class="container">
      <div class="row">
        <br />
        <a href="editionprofil.php">Editer mon profil</a><br />
        <a href="deconnexion.php">Se déconnecter</a>
        <br /><br />
        <h4>Informations sur l'utilisateur</h4>
        Pseudo = <?php echo $userinfo['pseudo']; ?>
        <br />
        Mail = <?php echo $userinfo['mail']; ?>
        <br />
        <?php
        if (isset($_SESSION['id']) and $userinfo['id'] == $_SESSION['id']) {
          ?>
          <br />
        </div>
        <?php if ($getAdmin == 1) {
          $tableauDonnées = getInformationsAdministrateur();
          ?>
          <div class="row">
            <h4>Tableau de bord administrateur</h4>
            <p> En tant qu'administrateur, vous avez accès à un certains nombre de données sur votre site <p>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>
                        Information
                      </th>
                      <th>
                        Valeur
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Nombre d'utilisateurs d'Alhambra</td>
                      <td><?php echo $tableauDonnées['nombre_utilisateurs'] ?></td>
                    </tr>
                    <tr>
                      <td>Nombre de voitures inscrites depuis la création d'Alhambra</td>
                      <td><?php echo $tableauDonnées['nombre_voiture_creation'] ?></td>
                    </tr>
                    <tr>
                      <td>Nombre de réservations depuis la création d'Alhambra</td>
                      <td><?php echo $tableauDonnées['nombre_reservation_creation'] ?></td>
                    </tr>
                    <tr>
                      <td>Nombre de location de voitures depuis la création d'Alhambra</td>
                      <td><?php echo $tableauDonnées['nombre_location_creation'] ?></td>
                    </tr>
                  </tbody>
                </table>
          </div>
          <div class="row">
            <h4>Ensemble des réservations en cours</h4>
            <?php if (sizeof($tableauReservaztionAdmin) > 0) { ?>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>
                      #
                    </th>
                    <th>
                      Aéroport
                    </th>
                    <th>
                      Site
                    </th>
                    <th>
                      Voiture
                    </th>
                    <th>
                      Date de debut
                    </th>
                    <th>
                      Date de fin
                    </th>
                    <th>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $index1 = 1;
                  foreach ($tableauReservaztionAdmin as $ligne) {
                    echo '
                    <tr>
                     <td>' . $index1 . '</td>
                     <td>' . $aeroportsNoms[$ligne[5] - 1] . '</td>
                     <td>' . getSite($ligne[6]) . '</td>
                     <td>' . getVoiture($ligne[2]) . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[4] . '</td>
                     <td>   <form action="" method = "POST">
                     <input type = "hidden" name = "idDeleteReservation" value = ' . $ligne[0] . '>
                     <input type = "hidden" name = "idVoitureDeleteReservation" value = ' . $ligne[2] . '>
                     <input type = "hidden" name = "dateDebutDeleteReservation" value = ' . $ligne[3] . '>
                     <input type = "hidden" name = "dateFinDeleteReservation" value = ' . $ligne[4] . '>
                  
                     <button type ="submit" Style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;"/> 
                     <i class="icon-trash"></i> </button></form></td>
                    ';
                    $index1++;
                  }
                  ?>
                </tbody>
              </table>
            <?php } else {
            echo "Vous n'avez pas de réservation !";
          } ?>
          </div>
          <div class="row">
            <h4>Ensemble des depôts en cours</h4>
            <?php if (sizeof($tableauAdminLocation) > 0) { ?>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>
                      #
                    </th>
                    <th>
                      Aéroport
                    </th>
                    <th>
                      Site
                    </th>
                    <th>
                      Voiture
                    </th>
                    <th>
                      Date de debut
                    </th>
                    <th>
                      Date de fin
                    </th>
                    <th>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
        
                  $index2 = 1;
                  foreach ($tableauAdminLocation as $ligne) {
                  
                    echo '
                    <tr>
                     <td>' . $index2 . '</td>
                     <td>' . $aeroportsNoms[$ligne[5] - 1] . '</td>
                     <td>' . getSite($ligne[6]) . '</td>
                     <td>' . getVoiture($ligne[2]) . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[4] . '</td>
                    
                    ';
                    $index2++;
                  }
                  ?>
                </tbody>
              </table>
            <?php } else {
            echo "Pas de location en cours";
          } ?>
          </div>
        <?php } else { ?>
          <div class="row">
            <h4>Mes réservations</h4>
            <?php if (sizeof($tableau) > 0) { ?>
              <p> Il est possible d'annuler des réservations, mais une pénalité de 50% du prix est encourue </p> <br>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>
                      #
                    </th>
                    <th>
                      Aéroport
                    </th>
                    <th>
                      Site
                    </th>
                    <th>
                      Voiture
                    </th>
                    <th>
                      Date de debut
                    </th>
                    <th>
                      Date de fin
                    </th>
                    <th>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $index1 = 1;
                  foreach ($tableau as $ligne) {
                    echo '
                    <tr>
                     <td>' . $index1 . '</td>
                     <td>' . $aeroportsNoms[$ligne[5] - 1] . '</td>
                     <td>' . getSite($ligne[6]) . '</td>
                     <td>' . getVoiture($ligne[2]) . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[4] . '</td>
                     <td>   <form action="" method = "POST">
                     <input type = "hidden" name = "idDeleteReservation" value = ' . $ligne[0] . '>
                     <input type = "hidden" name = "idVoitureDeleteReservation" value = ' . $ligne[2] . '>
                     <input type = "hidden" name = "dateDebutDeleteReservation" value = ' . $ligne[3] . '>
                     <input type = "hidden" name = "dateFinDeleteReservation" value = ' . $ligne[4] . '>
                     <input type = "hidden" name = "prixReservation" value = ' . $ligne['prix'] . '>
                     <button type ="submit" Style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;"/> 
                     <i class="icon-trash"></i> </button></form></td>
                    ';
                    $index1++;
                  }
                  ?>
                </tbody>
              </table>
            <?php } else {
            echo "Vous n'avez pas de réservation !";
          } ?>
          </div>
          <br> <br>
          <div class="row">
            <h4>Mes locations</h4>
            <?php if (sizeof($tableauLocation) > 0) { ?>
              <p> L'annulation de location sans poursuite est impossible afin de ne pas gêner les utilisateurs de Alhambra </p> <br>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>
                      #
                    </th>
                    <th>
                      Aéroport
                    </th>
                    <th>
                      Site
                    </th>
                    <th>
                      Voiture
                    </th>
                    <th>
                      Date de debut
                    </th>
                    <th>
                      Date de fin
                    </th>
                    <th>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $index2 = 1;
                  foreach ($tableauLocation as $ligne) {
                    echo '
                    <tr>
                     <td>' . $index2 . '</td>
                     <td>' . $aeroportsNoms[$ligne[5] - 1] . '</td>
                     <td>' . getSite($ligne[6]) . '</td>
                     <td>' . getVoiture($ligne[2]) . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[4] . '</td>
                     <td>   <form action="" method = "POST">
                     <input type = "hidden" name = "idDeleteLocation" value = ' . $ligne[0] . '>
                     <input type = "hidden" name = "idVoitureDeleteLocation" value = ' . $ligne[2] . '>
                     <input type = "hidden" name = "dateDebutDeleteLocation" value = ' . $ligne[3] . '>
                     <input type = "hidden" name = "dateFinDeleteLocation" value = ' . $ligne[4] . '>
                    
                     <button type ="submit" Style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;"/> 
                     <i class="icon-trash"></i> </button></form></td>
                    ';
                    $index2++;
                  }
                  ?>
                </tbody>
              </table>
            <?php } else {
            echo "Vous n'avez pas de location !";
          } ?>
          <?php
        }
      }
      ?>
      </div>
    </div>
    <br>
    <?php
    include 'footer.php';
    ?>
    </div>
    <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
    <?php
    include 'dependances.php';
    ?>
  </body>
  </html>
<?php
}
?>