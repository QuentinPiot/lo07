<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Alhambra</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="featured">
    <!-- start slider -->
    <div id="slider" class="sl-slider-wrapper demo-2">
      <div class="sl-slider">
        <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
          <div class="sl-slide-inner">
            <div class="bg-img bg-img-2">
            </div>
            <h2><strong>Louer</strong> des voitures à bas coût</h2>
            <blockquote>
              <p>
                Louer les voitures des usagers de Alhambra pour un prix défiant toute concurrence.
              </p>
            </blockquote>
          </div>
        </div>
        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
          <div class="sl-slide-inner">
            <div class="bg-img bg-img-1">
            </div>
            <h2>Parking<strong> gratuit</strong></h2>
            <blockquote>
              <p>
                Avec Alhambra, louer votre voiture vous permet de ne payer aucun frais de parking à l'Aéroport
              </p>
            </blockquote>
          </div>
        </div>
        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
          <div class="sl-slide-inner">
            <div class="bg-img bg-img-3">
            </div>
            <h2><strong>Présent</strong> partout dans le monde</h2>
            <blockquote>
     
            </blockquote>
          </div>
        </div>
      </div>
      <!-- /sl-slider -->
      <nav id="nav-dots" class="nav-dots">
        <span class="nav-dot-current"></span>
        <span></span>
        <span></span>
      </nav>
    </div>
    <!-- /slider-wrapper -->
    <!-- end slider -->
  </section>
  <section class="callaction">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="big-cta">
            <div class="cta-text">
              <h3>Nous sommes présents dans plus de <span class="highlight"><strong>10 aéroports</strong></span> ! </h3>
            </div>
            <div class="cta floatright">
              <a class="btn btn-large btn-theme btn-rounded" href=" reservation.php"> Réserver un véhicule</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="row">
            <div class="span3">
              <div class="box aligncenter">
                <div class="aligncenter icon">
                  <i class="icon-credit-card icon-circled icon-64 active"></i>
                </div>
                <div class="text">
                  <h6>Réserver</h6>
                  <p>
                    Vous pouvez réserver un véhicule dans tous les aéroports où Alhambra est présent.
                  </p>
                  <a href="reservation.php">Réserver un véhicule</a>
                </div>
              </div>
            </div>
            <div class="span3">
              <div class="box aligncenter">
                <div class="aligncenter icon">
                  <i class="icon-money icon-circled icon-64 active"></i>
                </div>
                <div class="text">
                  <h6>Économiser</h6>
                  <p>
                    En proposant votre véhicule à d'autres usagez, économisez sur votre place de parking.
                  </p>
                  <a href="proposerVehicule.php">Proposer son véhicule</a>
                </div>
              </div>
            </div>
            <div class="span3">
              <div class="box aligncenter">
                <div class="aligncenter icon">
                  <i class="icon-tablet icon-circled icon-64 active"></i>
                </div>
                <div class="text">
                  <h6>Multi-plateformes</h6>
                  <p>
                    Le site est également disponible sur smartphones et tablettes.
                  </p>
                </div>
              </div>
            </div>
            <div class="span3">
              <div class="box aligncenter">
                <div class="aligncenter icon">
                  <i class="icon-globe icon-circled icon-64 active"></i>
                </div>
                <div class="text">
                  <h6>International</h6>
                  <p>
                    Alhambra est présent dans de nombreux aéroports en France et dans le monde entier.
                  </p>
                  <a href="carte.php">Carte des aéroports</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- divider -->
      <div class="row">
        <div class="span12">
          <div class="solidline">
          </div>
        </div>
      </div>
      <!-- end divider -->
      <!-- Portfolio Projects -->
      <div class="row">
        <div class="span12">
          <h4 class="heading">Principaux <strong>aéroports</strong></h4>
          <div class="row">
            <section id="projects">
              <ul id="thumbs" class="portfolio">
                <!-- Item Project and Filter Name -->
                <li class="item-thumbs span3 design" data-id="id-0" data-type="web">
                  <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroports de Paris" href="public/img/works/thumbs/image-01.jpg">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <!-- Thumb Image and Description -->
                  <img src="public/img/works/thumbs/image-01.jpg" alt="Aéroports Charles De Gaulle et Orly">
                </li>
                <!-- End Item Project -->
                <!-- Item Project and Filter Name -->
                <li class="item-thumbs span3 design" data-id="id-1" data-type="icon">
                  <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Biarritz" href="public/img/works/thumbs/image-02.png">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <img src="public/img/works/thumbs/image-02.png" alt=" ">
                </li>
           
                <li class="item-thumbs span3 photography" data-id="id-2" data-type="illustrator">
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Toulouse-Blagnac" href="public/img/works/thumbs/image-03.png">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <img src="public/img/works/thumbs/image-03.png" alt=" ">
                </li>
                <li class="item-thumbs span3 photography" data-id="id-2" data-type="illustrator">
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Bordeaux" href="public/img/works/thumbs/image-04.png">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <img src="public/img/works/thumbs/image-04.png" alt=" ">
                </li>
               
                <li class="item-thumbs span3 photography" data-id="id-4" data-type="web">
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Malaga" href="public/img/works/thumbs/image-05.jpg">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <img src="public/img/works/thumbs/image-05.jpg" alt=" ">
                </li>
                <li class="item-thumbs span3 photography" data-id="id-5" data-type="icon">
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Séoul" href="public/img/works/thumbs/image-06.jpg">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <img src="public/img/works/thumbs/image-06.jpg" alt=" ">
                </li>
          
                <li class="item-thumbs span3 photography" data-id="id-2" data-type="illustrator">
                  <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Tokyo" href="public/img/works/thumbs/image-07.jpg">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <!-- Thumb Image and Description -->
                  <img src="public/img/works/thumbs/image-07.jpg" alt=" ">
                </li>
                <!-- End Item Project -->
                <!-- Item Project and Filter Name -->
                <li class="item-thumbs span3 design" data-id="id-0" data-type="web">
                  <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Aéroport de Rio de Janeiro" href="public/img/works/thumbs/image-08.jpg">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <!-- Thumb Image and Description -->
                  <img src="public/img/works/thumbs/image-08.jpg" alt=" ">
                </li>
                <!-- End Item Project -->
              </ul>
            </section>
          </div>
        </div>
      </div>
  
    </div>
  </section>
  <section id="bottom">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="aligncenter">
            <div id="twitter-wrapper">
              <div id="twitter">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>