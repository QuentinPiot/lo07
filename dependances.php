<?php 
echo '
<script src="public/js/jquery.js"></script>
<script src="public/js/jquery.easing.1.3.js"></script>
<script src="public/js/bootstrap.js"></script>
<script src="public/js/jcarousel/jquery.jcarousel.min.js"></script>
<script src="public/js/jquery.fancybox.pack.js"></script>
<script src="public/js/jquery.fancybox-media.js"></script>
<script src="public/js/google-code-prettify/prettify.js"></script>
<script src="public/js/portfolio/jquery.quicksand.js"></script>
<script src="public/js/portfolio/setting.js"></script>
<script src="public/js/jquery.flexslider.js"></script>
<script src="public/js/jquery.nivo.slider.js"></script>
<script src="public/js/modernizr.custom.js"></script>
<script src="public/js/jquery.ba-cond.min.js"></script>
<script src="public/js/jquery.slitslider.js"></script>
<script src="public/js/animate.js"></script>
<!-- Template Custom JavaScript File -->
<script src="public/js/custom.js"></script>
';