<?php
session_start();
require_once('date.php');
$liste = $_SESSION['listeVehicules'];
if (isset($_POST['idVehicule'])) {
  $_SESSION["Aeroport"] = $liste[$_POST['idVehicule']][1];

  $_SESSION["Voiture"] = $liste[$_POST['idVehicule']][3];
  $_SESSION['idMarques'] = $liste[$_POST['idVehicule']][0];
  $_SESSION['sitesVehicules'] = $liste[$_POST['idVehicule']][2];
  $_SESSION['Modele'] = $liste[$_POST['idVehicule']][8];
  header("Location: reservationSuccess.php");
}
$liste = $_SESSION['listeVehicules'];
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$arraySites = array();
foreach ($liste as $voiture) {
  $idSite = $voiture[2];
  $requete1 = "SELECT * FROM sites WHERE id LIKE $idSite";
  //echo $requete1;
  $requete_preparee1 = $bdd->prepare($requete1);
  $requete_preparee1->execute();
  while ($results = $requete_preparee1->fetch()) {
    array_push($arraySites, $results);
    // echo "yo";
  }
}
//print_r($arraySites);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Choix Véhicule</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Choix du véhicule</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <br>
  <div class="container">
    <h4>Listes des véhicules disponibles</h4>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>
            #
          </th>
          <th>
            Marque
          </th>
          <th>
            Modèle
          </th>
          <th>
            Parking
          </th>
          <th>
            Adresse
          </th>
          <th>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        $id = 1;
        foreach ($liste as $ligne) {
          $idVehicule = $ligne[0];
          echo '
                    <tr>
                     <td>' . $id . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[8] . '</td>
                     <td>' . $arraySites[$id - 1][3] . '</td>
                     <td>' . $arraySites[$id - 1][4] . '</td>
                     <td> <form action = "" method = "POST">
                     <input type = "hidden" name = "idVehicule" value =' . ($id - 1) . ' />
                     <button type ="submit"  class="btn btn-large btn-theme btn-rounded"/>  Réserver  </button></td></form>
                     </tr>';
          $id++;
        }
        ?>
      </tbody>
    </table>
  </div>
  <br>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>