<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <meta charset="utf-8">
   <title>Parking</title>
   <?php include 'classicHead.php' ?>
</head>
<body>
   <?php
   include 'header.php';
   ?>
   <!-- end header -->
   <section id="inner-headline">
      <div class="container">
         <div class="row">
            <div class="span4">
               <div class="inner-heading">
                  <h2>Réserver parking</h2>
               </div>
            </div>
            <div class="span8">
               <ul class="breadcrumb">
                  <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
                  <li><a href="#">Pages</a><i class="icon-angle-right"></i></li>
                  <li class="active">À propos</li>
               </ul>
            </div>
         </div>
      </div>
   </section>
   <section id="content">
      <div class="container">
         <div class="row">
            <div class="span6">
               <h2>Bienvenue sur <strong>Alhambra</strong></h2>
            </div>
         </div>
         <!-- end divider -->
         <div class="row">
            <div class="span12">
               <!------ Include the above in your HEAD tag ---------->
               <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
               <div class="flight-engine">
                  <div class="container">
                     <div class="tabing">
                        <ul>
                           <li><a class="active" href="#1"><i class="fa fa-plane" aria-hidden="true"></i> Flight</a>
                           </li>
                           <li><a href="#2"><i class="fa fa-plane" aria-hidden="true"></i> Flight</a>
                           </li>
                        </ul>
                        <div class="tab-content">
                           <div id="1" class="tab1 active">
                              <div class="flight-tab row">
                                 <div class="persent-one">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="dep" placeholder="From City or airport">
                                 </div>
                                 <div class="persent-one">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="arival" placeholder="To City or airport">
                                 </div>
                                 <div class="persent-one less-per">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="from-date1" placeholder="Depart">
                                 </div>
                                 <div class="persent-one less-per">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="to-date" placeholder="Returrn">
                                 </div>
                                 <div class="persent-one">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <div class="textboxstyle" id="passenger">01 Passenger</div>
                                 </div>
                                 <div class="persent-one less-btn">
                                    <input type="Submit" name="submit" value="Search" class="btn btn-info cst-btn" id="srch">
                                 </div>
                              </div>
                              <!-- flight tab -->
                           </div>
                           <!-- tab 1 -->
                           <div id="2" class="tab1">
                              <div class="flight-tab row">
                                 <div class="persent-one">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="dep" placeholder="From City or airport">
                                 </div>
                                 <div class="persent-one">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="arival" placeholder="To City or airport">
                                 </div>
                                 <div class="persent-one less-per">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="from-date1" placeholder="Depart">
                                 </div>
                                 <div class="persent-one less-per">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <input type="text" name="dep" class="textboxstyle" id="to-date" placeholder="Returrn">
                                 </div>
                                 <div class="persent-one">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <div class="textboxstyle" id="passenger">01 Passenger</div>
                                 </div>
                                 <div class="persent-one less-btn">
                                    <input type="Submit" name="submit" value="Search" class="btn btn-info cst-btn" id="srch">
                                 </div>
                              </div>
                              <!-- flight tab -->
                           </div>
                           <!-- tab 1 -->
                        </div>
                     </div>
                     <!-- tab content -->
                  </div>
                  <!-- tabbing -->
               </div>
            </div>
         </div>
      </div>
      <!-- divider -->
      <!-- end divider -->
      </div>
   </section>
   <?php
   include 'footer.php';
   ?>
   </div>
   <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
   <?php
   include 'dependances.php';
   ?>
</body>
</html>