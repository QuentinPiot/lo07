<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>À propos</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>À propos</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li><a href="#">Pages</a><i class="icon-angle-right"></i></li>
            <li class="active">À propos</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="span6">
          <h2>Bienvenue sur <strong>Alhambra</strong></h2>
          <p>
            Alhambra est une plateforme française d’autopartage entre voyageurs, lancée en 2019 par Thibault Gauffre et Quentin Piot. Elle propose aux voyageurs laissant leur véhicule dans un aéroport, de le louer à d’autres voyageurs. De ce service, trois solutions clé en main sont nées: des parkings gratuits, payants et un service location de voiture, disponibles aux aéroports, gares, port et en centres-villes..
          </p>
          <p>
            Alhambra propose des parking gratuits et une rémunération en cas de location de son véhicule. De plus, il est également possible aux utilisateurs de l'application de réserver des véhicules de différentes gammes.
          </p>
          <!--<p>
              Alii wisi phaedrum quo te, duo cu alia neglegentur. Quo nonumy detraxit cu, viderer reformidans ut eos, lobortis euripidis posidonium et usu. Sed meis bonorum minimum cu, stet aperiam qualisque eu vim, vide luptatum ei nec. Ei nam wisi labitur mediocrem.
              Nam saepe appetere ut, veritus graecis minimum no vim. Vidisse impedit id per.
            </p>-->
        </div>
        <div class="span6">
          <!-- start flexslider -->
          <div class="flexslider">
            <ul class="slides">
              <li>
                <img src="public/img/propos/voiturePolice.jpg" alt="" />
              </li>
              <li>
                <img src="public/img/propos/multipla.jpg" alt="" />
              </li>
              <li>
                <img src="public/img/propos/delorean.jpg" alt="" />
              </li>
            </ul>
          </div>
          <!-- end flexslider -->
        </div>
      </div>
      <!-- divider -->
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>

  <?php
  include 'dependances.php';
  ?>
</body>
</html>