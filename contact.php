<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Contact</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Nous contacter</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li class="active">Nous contacter</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d23099.604107059622!2d-1.5236460923986712!3d43.464732738237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sfr!4v1553353313090" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="container">
      <div class="row">
        <div class="span12">
          <h4>Pour nous contacter, remplissez <strong>le formulaire ci-dessous</strong></h4>
          <form action="index.php" method="post" role="form" class="contactForm">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <div class="row">
              <div class="span4 form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Votre Nom" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="span4 form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Votre Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
              <div class="span4 form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Sujet" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="span12 margintop10 form-group">
                <textarea class="form-control" name="message" rows="12" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validation"></div>
                <p class="text-center">
                  <button class="btn btn-large btn-theme margintop10" type="submit">Envoyer le message</button>
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <script src="contactform\contactform.js"></script>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>