<?php
echo '<script src="public/js/jquery-3.2.1.min.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="public/js/jquery-migrate.min.js"></script>

<!--=== Bootstrap Min Js ===-->
<script src="public/js/bootstrap.min2.js"></script>
<!--=== Gijgo Min Js ===-->
<script src="public/js/plugins/gijgo.js"></script>
<!--=== Vegas Min Js ===-->
<script src="public/js/plugins/vegas.min.js"></script>

<!--=== Owl Caousel Min Js ===-->
<script src="public/js/plugins/owl.carousel.min.js"></script>

<!--=== CounTotop Min Js ===-->
<script src="public/js/plugins/counterup.min.js"></script>
<!--=== YtPlayer Min Js ===-->
<script src="public/js/plugins/mb.YTPlayer.js"></script>
<!--=== Magnific Popup Min Js ===-->
<script src="public/js/plugins/magnific-popup.min.js"></script>
<!--=== Slicknav Min Js ===-->
<script src="public/js/plugins/slicknav.min.js"></script>
<!--=== Mian Js ===-->
<script src="public/js/main.js"></script>
';
