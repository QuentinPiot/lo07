<?php
echo '
<footer>
      <div class="container">
        <div class="row">
          <div class="span3">
            <div class="widget">
              <h5 class="widgetheading">Alhambra</h5>
              <ul class="link-list" style="font-size : 12px">
                <li><a href="about.php">A propos de nous</a></li>
                
                <li><a href="equipe.php">L&#039;Équipe</a></li>
                <li><a href="contact.php">Nous contacter</a></li>
              </ul>
            </div>
          </div>
          <div class="span3">
            <div class="widget">
              <h5 class="widgetheading">Aéroports</h5>
              <ul class="link-list" style="font-size : 12px">
                
                <li>Charles de Gaulle</li>
                <li>Orly</li>
                <li>Biarritz</li>
                <li>Toulouse</li>
                <li>Bordeaux</li>
                <li>Malaga</li>
                <li>Séoul</li>
                <li>Tokyo</li>
                <li>Rio de Janeiro</a></li>
              
              </ul>
            </div>
          </div>
         <!-- <div class="span3">
            <div class="widget">
              <h5 class="widgetheading">Flickr photostream</h5>
              <div class="flickr_badge">
                <script type="text/javascript" src="http://www.flickr.com/badge_Écode_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
              </div>
              <div class="clear">
              </div>
            </div>
          </div>-->
          <div class="span3">
            <div class="widget" style="font-size : 12px">
              <h5 class="widgetheading">Nous contacter </h5>
              <address>
								<strong>Biarritz</strong><br>
								Nouvelle-Aquitaine<br>
								 France
					 		</address>
              <p>
                <i class="icon-phone"></i> 05 59 00 00 00 <br>
                <i class="icon-envelope-alt"></i> contact@alhambra.eu
              </p>
            </div>
          </div>
        </div>
      </div>
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p>
                  <span>&copy; Alhambra - All right reserved.</span>
                </p>
                <div class="credits">
                  <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Flattern
                  
                  Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>-->
                </div>
              </div>
            </div>
            <div class="span6">
              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-square"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-square"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-square"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest icon-square"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google plus"><i class="icon-google-plus icon-square"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>';
