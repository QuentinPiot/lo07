<meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="public/css/bootstrap.css" rel="stylesheet" />
  <link href="public/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="public/css/fancybox/jquery.fancybox.css" rel="stylesheet">
  <link href="public/css/jcarousel.css" rel="stylesheet" />
  <link href="public/css/flexslider.css" rel="stylesheet" />
  <link href="public/css/slitslider.css" rel="stylesheet" />
  <link href="public/css/style.css" rel="stylesheet" />
  <!-- Theme skin -->
  <link id="t-colors" href="public/css/skins/blue.css" rel="stylesheet" />
  <!-- boxed bg -->
  <link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="public/img/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="public/img/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="public/img/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="public/img/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="public/img/ico/favicon.png" />
