<?php
session_start();
require_once('date.php');
require_once('requete.php');

$Requete = new Requeteobjet( new PDO('mysql:host=localhost;dbname=Alhambra', 'root', ''));
if (!empty($_POST['indexSite'])) {
  $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
  $idC = $_SESSION['id'];
  $idS =  $_POST['indexSite'];
  $idA = $_SESSION['AeroportLouer'];
  $modeleV = $_SESSION['VoitureLouer'];
  $stD = $_SESSION['startDate2Louer'];
  $stF = $_SESSION['endDate2Louer'];
  $mV =   $_SESSION['ModeleVoiture'];
  $reservation = "{}";
  $valeurs1 = [
    'vidS' => $idS,
    'vidA' => $idA,
    'vmodeleV' => $modeleV,
    'vstD' => $stD,
    'vstF' => $stF,
    'vidC' => $idC,
    'vmV' =>  $mV
  ];
  $Requete->requeteSimple( "INSERT INTO voiture (aeroport, site, marque, dateDebut, dateFin,reservation, idClient, modele)
  VALUES (:vidA,:vidS,:vmodeleV,:vstD,:vstF,'{}', :vidC, :vmV)",$valeurs1);

  $requeteID = "SELECT * FROM voiture WHERE idClient LIKE $idC AND dateDebut LIKE  '$stD' AND dateFin LIKE '$stF' AND site LIKE $idS";
  $requete_prepareeID = $bdd->prepare($requeteID);
  $requete_prepareeID->execute();
  while ($results = $requete_prepareeID->fetch()) {
    $idVehicule =  $results[0];
  }
  $valeurs2 = [
    'vidS' => $idS,
    'vidA' => $idA,
    'vidC' => $idC,
    'vidV' => $idVehicule,
    'vstD' => $stD,
    'vstF' => $stF
  ];
  $Requete->requeteSimple( "INSERT INTO location (client, voitureID, dateDebut, dateFin, aeroport, site)
    VALUES (:vidC, :vidV,:vstD,:vstF,:vidA,:vidS)",$valeurs2);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Deposer</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
  $aeroportsNoms = array('Biarritz', 'Orly', 'Charles De Gaulle', 'Bordeaux', 'Toulouse', 'Malaga', 'Séoul', 'Tokyo', 'Rio de Janeiro');
  $aeroportChoisi = $_SESSION['AeroportLouer'];
  $tableau = array();
  $requete1 = "SELECT * FROM sites WHERE disponible LIKE 0 AND aeroport LIKE $aeroportChoisi";
  $requete_preparee1 = $bdd->prepare($requete1);
  $requete_preparee1->execute();
  if (!$requete_preparee1->rowCount() == 0) {
    while ($results = $requete_preparee1->fetch()) {
      array_push($tableau, $results);
    }
  } else {
    //echo 'Nothing found';
  };
  $numero = 0;
  foreach ($tableau as $value) {
    $index = 0;
    $tabVoitures = array();
    $requete1 = "SELECT * FROM voiture WHERE site = $value[0]";
    $requete_preparee2 = $bdd->prepare($requete1);
    $requete_preparee2->execute();
    if (!$requete_preparee2->rowCount() == 0) {
      while ($results2 = $requete_preparee2->fetch()) {
        array_push($tabVoitures, $results2);
      }
    } else {
      //echo 'Nothing found';
    };
    foreach ($tabVoitures as $voiture) {
      $dateDebut = new ObjetDate($voiture[4]);
      $dateFin = new ObjetDate($voiture[5]);
      $dateDebut = $dateDebut->changerFormat();
      $dateFin = $dateFin->changerFormat();
      $dateDClient = new ObjetDate($_SESSION['startDate2Louer']);
      $dateDFClient = new ObjetDate($_SESSION['endDate2Louer']);
      $dateDClient = $dateDClient->changerFormat();
      $dateDFClient = $dateDFClient->changerFormat();
      $intervalDCFin = $dateDClient->diff($dateFin);
      $intervalDCFin = $intervalDCFin->format('%R%a');
      $intervalDFDebut = $dateDFClient->diff($dateDebut);
      $intervalDFDebut = $intervalDFDebut->format('%R%a');
      if ($intervalDCFin > 0 and $intervalDFDebut < 0) {
        $index++;
      }
    }
    $tableau[$numero][6] = $tableau[$numero][5] - $index;
    $numero++;
  }
  include 'header.php';
  ?>
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Déposer un véhicule</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <br>
  <div class="container">
    <?php if (empty($_POST['indexSite'])) { ?>
      <h4>Listes des sites</h4>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>
              #
            </th>
            <th>
              Aéroport
            </th>
            <th>
              Lieu
            </th>
            <th>
              Parking
            </th>
            <th>
              Adresse
            </th>
            <th>
              Places totales
            </th>
            <th>
              Place disponibles
            </th>
            <th>
              Louer
            </th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($tableau as $ligne) {
            echo '
                    <tr>
                     <td>' . $ligne[0] . '</td>
                     <td>' . $aeroportsNoms[$ligne[1] - 1] . '</td>
                     <td>' . $ligne[2] . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[4] . '</td>
                     <td>' . $ligne[5] . '</td>
                     <td>' . $ligne[6] . '</td>';
            if ($ligne[6] > 0) {
              echo '<td> 
                     <form action = "#Validation" method = "POST">
                      <input type = "hidden" name = "indexSite" value =' . $ligne[0] . ' />
                      <input type = "hidden" name = "indexAeroport" value =' . ($ligne[1]) . ' />';
              if ($ligne[6] > 0) echo '
                      <button type ="submit"  class="btn btn-large btn-theme btn-rounded"/>  Choisir  </button></td></form>';
            } else {
              echo ' <td> Plus de place </td> </tr>
                       ';
            }
          }
          ?>
        </tbody>
      </table>
      <br>
    <?php } else {
    echo "<br><br><p> La demande a bien été prise en compte ... <p><br><br>";
  }
  ?>
  </div>
  <?php
  include 'footer.php';
  ?>
  </div>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>