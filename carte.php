<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Carte</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Carte</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li><a href="#">Pages</a><i class="icon-angle-right"></i></li>
            <li class="active">Carte</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="span6">
          <h2>Carte des aéroports <strong>partenaires</strong></h2>
        </div>
      </div>
      <!-- end divider -->
      <div class="row">
        <div class="span12">
          <iframe src="https://www.google.com/maps/d/embed?mid=10d-Z-1fHzsRAOy7Ytp3J61zf7uIZNmt8" width="100%" height="800"></iframe>
        </div>

      </div>
      <!-- divider -->
      <div class="row">
        <div class="span12">
          <div class="solidline">
          </div>
        </div>
      </div>
      <!-- end divider -->
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>