<?php
session_start();
require_once('date.php');

?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Réservation</title>
  <?php include 'classicHead.php';
  include 'rentHead.php'; ?>
</head>
<body>
  <?php
  function availableVehicle($aeroport)
  {
   $_SESSION["startDate2"] = $_GET["startDate2"];
    $_SESSION["endDate2"] = $_GET["endDate2"];
    $dateDebut =  new ObjetDate($_GET['startDate2']);
    $dateFin = new ObjetDate($_GET['endDate2']);
    $dateDebut = $dateDebut->changerFormat();
    $dateFin = $dateFin->changerFormat();
    //print_r("voiture : ".$dateDebut." - ".$
    $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
    $array_marque = array("\"Peugeot\"", "\"Renault\"", "\"Nissan\"", "\"Audi\"", "\"Alfa Romeo\"", "\"Tesla\"");

    $idClient = $_SESSION['id'];
    $tableauVehicules = array();
    $tableauVehiculesFinal = array();
    foreach ($array_marque as $value) {
      
      $requete1 = "SELECT * FROM voiture WHERE aeroport LIKE $aeroport AND marque LIKE $value AND idClient <> $idClient";
      //echo $requete1;
      $requete_preparee1 = $bdd->prepare($requete1);
      $requete_preparee1->execute();
      if (!$requete_preparee1->rowCount() == 0) {
        while ($results = $requete_preparee1->fetch()) {
          array_push($tableauVehicules, $results);
        }
      } else {
        // echo 'Nothing found';
      };
    }

    foreach ($tableauVehicules as $key => $value) {
      $dateDebutVehicule =  new ObjetDate($value['dateDebut']);
      $dateFinVehicule = new ObjetDate($value['dateFin']);
      $dateDebutVehicule = $dateDebutVehicule->changerFormat();
      $dateFinVehicule = $dateFinVehicule->changerFormat();
      $intervalFinVFin = $dateFin->diff($dateFinVehicule);
      $intervalFinVFin = $intervalFinVFin->format('%R%a');
      $intervalDebutVDebut = $dateDebut->diff($dateDebutVehicule);
      $intervalDebutVDebut = $intervalDebutVDebut->format('%R%a');
   
      if ($intervalFinVFin >= 0 && $intervalDebutVDebut <= 0) {
       
        if ($value[6] == "{}") {
          
          array_push($tableauVehiculesFinal, $value);
     
        } else {
          $tabReservations = json_decode($value[6]);
          $disponible = true;
          foreach ($tabReservations as $valueReservation) {
            $dateDebutReservation = new ObjetDate($valueReservation[0]);
            $dateDebutReservation = $dateDebutReservation->changerFormat();
            $dateFinReservation = new ObjetDate($valueReservation[1]);
            $dateFinReservation = $dateFinReservation->changerFormat();
         
            $intervalDebutVFinR = $dateDebut->diff($dateFinReservation);
            $intervalDebutVFinR = $intervalDebutVFinR->format('%R%a');
            $intervalFinVDebutR = $dateFin->diff($dateDebutReservation);
            $intervalFinVDebutR = $intervalFinVDebutR->format('%R%a');
            if (!($intervalDebutVFinR <0 or $intervalFinVDebutR > 0)) {
              $disponible = false;
              break;
            } 
          }
          if($disponible)   array_push($tableauVehiculesFinal, $value);
        }
      }
    }
   
    $_SESSION['listeVehicules'] = $tableauVehiculesFinal;
  header("Location: chooseVehicle.php");
  }
  if (!empty($_GET['startDate2']) and !empty($_GET['endDate2'])) {
    $resultAvailableVehicles = availableVehicle($_GET['Aeroport']);
    $vehiculesDisponibles = $resultAvailableVehicles[0];
    $tableauIndex = $resultAvailableVehicles[1];
    $tableauSite = $resultAvailableVehicles[2];
  }
  if (!empty($_GET['Aeroport'])) {
    $nomAeroport = array("Biarritz", "Orly", "CDG", "Bordeaux", "Toulouse", "Malaga", "Séoul", "Tokyo", "Rio");
    $idAeroport = $_GET['Aeroport'];
    $aeroport = $nomAeroport[$idAeroport - 1];
  }
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Réservation</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="index.php"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li><a href="index.php">Pages</a><i class="icon-angle-right"></i></li>
            <li class="active">Réservation</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <script>
  </script>
  <?php if (isset($_SESSION['id'])) {
    $peuxChoisir = false;
    if (!empty($_GET['endDate2']) and !empty($_GET['startDate2'])) {
      $peuxChoisir = true;
    }
    ?>
    <section id="slideslow-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <div class="slideshowcontent">
              <div class="display-table">
                <div class="display-table-cell">
                  <?php
                  if (!isset($_GET["startDate2"]) and !isset($_GET["endDate2"])  and !isset($_GET["Voiture"])) {
                    echo "<br>
                                Veuillez remplir l'ensemble des champs<br>Le choix du véhicule sera réalisé en fonction des disponibilités
                                <br>";
                  } else {
                    if (empty($_GET["startDate2"])) echo "<p style ='color : white; text-shadow: 1px 1px 2px black;'>Veuillez rentrer une date de départ</p><br>";
                    if (empty($_GET["endDate2"])) echo "<p style ='color : white; text-shadow: 1px 1px 2px black;'>Veuillez rentrer une date d'arrivé</p><br>";
                    // if(isset($_GET["Voiture"]))  if($_GET["Voiture"] < 1) echo "<p style ='color : red; text-shadow: 1px 1px 2 px black;'>Veuillez choisir une voiture</p><br>";
                  }
                  ?>
                  <div class="book-ur-car" style="width : 90%">
                    <form action="#send" method="GET">
                      <div class='pick-location bookinput-item' style='width : 22%'>
                        <?php if (!$peuxChoisir) { ?>
                          <select class="custom-select" name="Aeroport" id="AeroportSelect">
                            <option value="1" selected>Biarritz</option>
                            <option value="2">Paris Orly</option>
                            <option value="3">Paris CDG</option>
                            <option value="4">Bordeaux</option>
                            <option value="5">Toulouse</option>
                            <option value="6">Malaga</option>
                            <option value="7">Séoul</option>
                            <option value="8">Tokyo</option>
                            <option value="9">Rio</option>
                          </select>
                        <?php } else {
                        echo ('<input type = "hidden" name = "Aeroport" value ="' . $idAeroport . '"/>
                                          <input type = "hidden" name = "idMarques" value =' . serialize($tableauIndex) . '/>
                                          <input type = "hidden" name = "sitesVehicules" value =' . serialize($tableauSite) . '/>
                                          <input type = "text" value =' . $aeroport . ' disabled />');
                      }
                      ?>
                      </div>
                      <div class="pick-date bookinput-item" style='width : 22%'>
                        <input <?php if (!$peuxChoisir) echo ('id="startDate2"'); ?> placeholder="Date de début" name="startDate2" <?php if ($peuxChoisir) ?> <?php if (!empty($_GET['startDate2'])) echo ('value = ' . $_GET['startDate2']); ?> />
                      </div>
                      <div class="retern-date bookinput-item" style='width : 22%'>
                        <input <?php if (!$peuxChoisir) echo ('id="endDate2"'); ?> placeholder="Date de fin" name="endDate2" <?php if ($peuxChoisir); ?> <?php if (!empty($_GET['endDate2'])) echo ('value = ' . $_GET['endDate2']); ?> />
                      </div>
                  
                      <div class="bookcar-btn bookinput-item" style='width : 22%'>
                        <button type="submit" name="submit" value="upload"><?php
                                                                            if ($peuxChoisir) echo 'Réserver une voiture';
                                                                            else echo 'Choisir un véhicule'; ?></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } else {
  echo "Vous devez être connecté pour accéder à cette page";
} ?>
  <?php
  include 'footer.php';
  ?>
  </div>
  <?php
  include 'dependances.php';
  include 'dependancesRent.php';
  ?>
</body>
</html>