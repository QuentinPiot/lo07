<?php
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$erreurMessage = "";
if (isset($_SESSION['id'])) {
  $requser = $bdd->prepare("SELECT * FROM membres WHERE id = ?");
  $requser->execute(array($_SESSION['id']));
  $user = $requser->fetch();
  if (isset($_POST['newpseudo']) and !empty($_POST['newpseudo']) and $_POST['newpseudo'] != $user['pseudo']) {
    $newpseudo = htmlspecialchars($_POST['newpseudo']);
    $insertpseudo = $bdd->prepare("UPDATE membres SET pseudo = ? WHERE id = ?");
    $insertpseudo->execute(array($newpseudo, $_SESSION['id']));
    header('Location: profil.php?id=' . $_SESSION['id']);
  }
  if (!isset($_POST['newmail']) and !empty($_POST['newmail']) and $_POST['newmail'] != $user['mail']) {
    $newmail = htmlspecialchars($_POST['newmail']);
    $insertmail = $bdd->prepare("UPDATE membres SET mail = ? WHERE id = ?");
    $insertmail->execute(array($newmail, $_SESSION['id']));
    header('Location: profil.php?id=' . $_SESSION['id']);
  }
  if (!isset($_POST['newmail']) && isset($_POST['newpseudo'])) $erreurMessage .= "Email invalide<br>";
  if (isset($_POST['newmdp1']) and !empty($_POST['newmdp1']) and isset($_POST['newmdp2']) and !empty($_POST['newmdp2'])) {
    $mdp1 = sha1($_POST['newmdp1']);
    $mdp2 = sha1($_POST['newmdp2']);
    if ($mdp1 == $mdp2) {
      $insertmdp = $bdd->prepare("UPDATE membres SET motdepasse = ? WHERE id = ?");
      $insertmdp->execute(array($mdp1, $_SESSION['id']));
      header('Location: profil.php?id=' . $_SESSION['id']);
    } else if (isset($_POST['newpseudo'])) {
      $erreurMessage = "Les deux mots de passe ne correspondent pas<br>";
    }
  }
  if (!isset($mdp2) && isset($_POST['newpseudo'])) $erreurMessage .= "Veuillez rentrer la confirmation<br>";
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Edition</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Edition</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li class="active">Edition Profil</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="span12">
          Modification des informations du profil utilisateur
        </div>
      </div>
      <div class="row">
        <?php
        if (isset($erreurMessage)) {
          echo '<p class="text-error" style= "font-size : 20px">' . $erreurMessage . "</p>";
        }
        ?>
        <form method="POST" action="" enctype="multipart/form-data">
          <label>Pseudo :</label>
          <input type="text" name="newpseudo" placeholder="Pseudo" value="<?php echo $user['pseudo']; ?>" /><br /><br />
          <label>Mail :</label>
          <input type="text" name="newmail" placeholder="Mail" value="<?php echo $user['mail']; ?>" /><br /><br />
          <label>Mot de passe :</label>
          <input type="password" name="newmdp1" placeholder="Mot de passe" /><br /><br />
          <label>Confirmation - mot de passe :</label>
          <input type="password" name="newmdp2" placeholder="Confirmation du mot de passe" /><br /><br />
          <input type="submit" value="Confirmer" />
        </form>
      </div>
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>