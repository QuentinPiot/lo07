<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>À propos</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Équipe</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li><a href="#">Pages</a><i class="icon-angle-right"></i></li>
            <li class="active">À propos</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="span6">
          <h2>Bienvenue sur <strong>Alhambra</strong></h2>
        </div>
      </div>
      <!-- end divider -->
      <div class="row">
        <div class="span12">
          <h4>L'Équipe</h4>
        </div>
        <div class="span3">
          <img src="public/img/dummies/team3.jpg" alt="" class="img-polaroid" />
          <div class="roles">
            <p class="lead">
              <strong>Thibault Gauffre</strong>
            </p>
            <p>
              Co-Fondateur
            </p>
          </div>
        </div>
        <div class="span3">
          <img src="public/img/dummies/team2.jpg" alt="" class="img-polaroid" />
          <div class="roles">
            <p class="lead">
              <strong>Quentin Piot</strong>
            </p>
            <p>
              Co-Fondateur
            </p>
          </div>
        </div>
      </div>
      <!-- divider -->
      <div class="row">
        <div class="span12">
          <div class="solidline">
          </div>
        </div>
      </div>
      <!-- end divider -->
      <div class="row">
        <div class="span6">
          <h4>Nos compétences</h4>
          <label>Talent :</label>
          <div class="progress progress-info progress-striped active">
            <div class="bar" style="width: 90%">
            </div>
          </div>
          <label>Astrophysique :</label>
          <div class="progress progress-success progress-striped active">
            <div class="bar" style="width: 95%">
            </div>
          </div>
          <label>NetScape :</label>
          <div class="progress progress-warning progress-striped active">
            <div class="bar" style="width: 10%">
            </div>
          </div>
          <label>Modestie :</label>
          <div class="progress progress-danger progress-striped active">
            <div class="bar" style="width: 100%">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>