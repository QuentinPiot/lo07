<?php
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$erreur = null;
if(isset($_POST['formconnexion'])) {
   $mailconnect = htmlspecialchars($_POST['mailconnect']);
   $mdpconnect = sha1($_POST['mdpconnect']);
   if(!empty($mailconnect) AND !empty($mdpconnect)) {
      $requser = $bdd->prepare("SELECT * FROM membres WHERE mail = ? AND motdepasse = ?");
      $requser->execute(array($mailconnect, $mdpconnect));
      $userexist = $requser->rowCount();
      if($userexist == 1) {
         $userinfo = $requser->fetch();
         $_SESSION['id'] = $userinfo['id'];
         $_SESSION['pseudo'] = $userinfo['pseudo'];
         $_SESSION['mail'] = $userinfo['mail'];
         $_SESSION['admin'] = $userinfo['admin'];
         $_SESSION['argent'] = $userinfo['argent'];
         header("Location: profil.php?id=".$_SESSION['id']);
      } else {
        
         $erreur = "Mauvais mail ou mot de passe !";
       
      }
   } else {
      $erreur = "Tous les champs doivent être complétés !";
   }
}
echo '  <div id="wrapper">
<!-- toggle top area -->
<div class="hidden-top">
  <div class="hidden-top-inner container">
    <div class="row">
      <div class="span12">
        <ul>
          <li><strong>Vous pouvez nous contacter dans tous les aéroports où nous sommes présents</strong></li>
          <li>Siège social: Biarritz</li>
          <li>Téléphone <i class="icon-phone"></i> 05 59 00 00 00</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- end toggle top area -->
<!-- start header -->
<header>
  <div class="container">
    <!-- hidden top area toggle link -->
    <div id="header-hidden-link">
      <a href="#" class="toggle-link" title="Click me you&#039;ll get a surprise" data-target=".hidden-top"><i></i>Open</a>
    </div>
    <!-- end toggle link -->
    <div class="row nomargin">
      <div class="span12">
        <div class="headnav">
          <ul>
          ';
          if(!isset($_SESSION['id'])) {
              echo ' <li><a href="inscription.php"><i class="icon-user"></i> Inscription</a></li>
              <li><a href="#mySignin" id = "connectClick" data-toggle="modal">Connexion</a></li>';
          }else {
            echo ' <li>Argent : '.$_SESSION['argent'].'€ | <a href ="profil.php?id='.$_SESSION['id'].'"> Profil </a> |<a href="deconnexion.php">  Se déconnecter </a><i class="icon-signout"></i></li>';
          }
           echo '
          </ul>
        </div>
        <!-- Signup Modal -->
        <div id="mySignup" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="mySignupModalLabel">Créer un compte<strong> Alhambra</strong></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="control-group">
                <label class="control-label" for="inputEmail">Email</label>
                <div class="controls">
                  <input type="text" id="inputEmail" placeholder="Email">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputSignupPassword">Password</label>
                <div class="controls">
                  <input type="password" id="inputSignupPassword" placeholder="Password">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputSignupPassword2">Confirm Password</label>
                <div class="controls">
                  <input type="password" id="inputSignupPassword2" placeholder="Password">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn">Sign up</button>
                </div>
                <p class="aligncenter margintop20">
                  Already have an account? <a href="#mySignin" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Sign in</a>
                </p>
              </div>
            </form>
          </div>
        </div>
        <!-- end signup modal -->
        <!-- Sign in Modal -->
        <div id="mySignin" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="mySigninModalLabel">Se connecter</h4>
          </div>
          <div class="modal-body">
           
            <form class="form-horizontal" method="POST" action="">
              <div class="control-group">
                <label class="control-label" for="inputText">Email</label>
                <div class="controls">
                  <input type="email" name="mailconnect" placeholder="Email" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputSigninPassword">Password</label>
                <div class="controls">
                  <input type="password" name="mdpconnect" placeholder="Mot de passe" />';
                 
         if(isset($erreur)) {
            echo '<p>  <font color="red">'.$erreur."</font></p>";
         }
        
            echo '               
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn" name="formconnexion" value="Se connecter !" />Se connecter</button>
                </div>
                <p class="aligncenter margintop20">
                  Mot de passe oublié ? <a href="#myReset" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Reset</a>
                </p>
              </div>
            </form>
          </div>
        </div>
        <!-- end signin modal -->
        <!-- Reset Modal -->
        <div id="myReset" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="myResetModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myResetModalLabel">Reset your <strong>password</strong></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="control-group">
                <label class="control-label" for="inputResetEmail">Email</label>
                <div class="controls">
                  <input type="text" id="inputResetEmail" placeholder="Email">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" id = "btnS" class="btn">Reset password</button>
                </div>
                <p class="aligncenter margintop20">
                  We will send instructions on how to reset your password to your inbox
                </p>
              </div>
            </form>
          </div>
        </div>
        <!-- end reset modal -->
      </div>
    </div>
    <div class="row">
      <div class="span4">
        <div class="logo">
          <a href="index.php"><img src="public/img/logo.png" alt="" class="logo" /></a>
          <h1>Économisez-vous la vie</h1>
        </div>
      </div>
      <div class="span8">
        <div class="navbar navbar-static-top">
          <div class="navigation">
            <nav>
              <ul class="nav topnav">
                <li class="dropdown active">
                  <a href="index.php">Accueil </i></a>
                  
                </li>
                <li class="dropdown">
                  <a href="reservation.php">Réserver </i></a>
                
                </li>
                <li class="dropdown">
                  <a href="proposerVehicule.php">Déposer son véhicule<i  ></i></a>
                  </li>
                ';
                if(isset($_SESSION['admin'])){
                    if($_SESSION['admin'] == 1){
                      echo ' <li class="dropdown">
                      <a href="site.php">Louer un site<i></i></a>
                     
                      </li>';
                    }
                }
                echo'
               
                <li class="dropdown">
                  <a href="carte.php">Carte aéroports</i></a>
                 
                </li>
                <li class="dropdown">
                  <a href="#">Alhambra <i class="icon-angle-down"></i></a>
                  <ul class="dropdown-menu">
                    <li><a href="about.php">À propos</a></li>
                    <li><a href="equipe.php">L&#039;Équipe</a></li>
                    <!--<li><a href="post-left-sidebar.php">Post left sidebar</a></li>
                    <li><a href="post-right-sidebar.php">Post right sidebar</a></li>-->
                  </ul>
                </li>
                
              </ul>
            </nav>
          </div>
          <!-- end navigation -->
        </div>
      </div>
    </div>
  </div>
  ';

  if(isset($erreur)) {
    echo '<br><p style="text-align:center;">  <font color="red">'.$erreur."<br> Veuillez Réessayer</font></p> <br> <br> <br>";
 }
 echo '
</header>';
?>
<script>
   
   $('formconnexion').submit(function (evt) {
   evt.preventDefault(); //prevents the default action
}
    
    </script>