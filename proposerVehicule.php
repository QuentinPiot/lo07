<?php
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$tabVehicules = array('Peugeot', 'Renault', 'Nissan', 'Audi', 'Alfa Romeo', 'Tesla');
if (!empty($_POST['startDate2']) and !empty($_POST['endDate2']) and !empty($_POST['Voiture']) and !empty($_POST['modeleVoiture'])) {
  $_SESSION['AeroportLouer'] = $_POST['Aeroport'];
  $_SESSION['startDate2Louer'] = $_POST['startDate2'];
  $_SESSION['endDate2Louer'] = $_POST['endDate2'];
  $_SESSION['VoitureLouer'] = $tabVehicules[$_POST['Voiture'] - 1];
  $_SESSION['ModeleVoiture'] = $_POST['modeleVoiture'];
  header("Location: louerVehicule.php");
}
$requete2 = "SELECT * FROM sites";
$requete_preparee2 = $bdd->prepare($requete2);
$requete_preparee2->execute();
$aeroportsNoms = array('Biarritz', 'Orly', 'CDG', 'Bordeaux', 'Toulouse', 'Malaga', 'Séoul', 'Tokyo', 'Rio');
if (!$requete_preparee2->rowCount() == 0) {
  while ($results = $requete_preparee2->fetch()) {
    $nomAeroport = $aeroportsNoms[$results[1] - 1] . " " . $results[2] . " " . $results[3];
  }
} else {
  //echo 'Nothing found';
};
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Dépôt</title>
  <?php include 'classicHead.php';
  include 'rentHead.php'; ?>
  <link href="assets/css/responsive.css" rel="stylesheet">
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Dépôt de véhicule</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
  </script>
  <?php if (isset($_SESSION['id'])) {
    $peuxChoisir = false;
    if (!empty($_GET['endDate2']) and !empty($_GET['startDate2'])) {
      $peuxChoisir = true;
    }
    ?>
    <section id="slideslow-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <div class="slideshowcontent">
              <div class="display-table">
                <div class="display-table-cell">
                  <?php
                  if (!isset($_POST["startDate2"]) and !isset($_POST["endDate2"])  and !isset($_POST["Voiture"]) and !isset($_POST['modeleVoiture'])) {
                    echo "<br>
                                Veuillez remplir l'ensemble des champs<br>Le choix du site sera réalisé ensuite
                                <br>";
                  } else {
                    if (empty($_POST["startDate2"])) echo "<p style ='color : white; text-shadow: 1px 1px 2px black;'>Veuillez rentrer une date de départ</p><br>";
                    if (empty($_POST["endDate2"])) echo "<p style ='color : white; text-shadow: 1px 1px 2px black;'>Veuillez rentrer une date d'arrivé</p><br>";
                    if (empty($_POST['modeleVoiture'])) echo "<p style ='color : white; text-shadow: 1px 1px 2px black;'>Veuillez rentrer le modèle de voiture</p><br>";
                    // if(isset($_POST["Voiture"]))  if($_POST["Voiture"] < 1) echo "<p style ='color : white; text-shadow: 1px 1px 2px red;'>Veuillez choisir une voiture</p><br>";
                  }
                  ?>
                  <div class="book-ur-car">
                    <form action="" method="POST">
                      <div class="pick-location bookinput-item">
                        <select class="custom-select" name="Aeroport" id="AeroportSelect">
                          <option value="1" selected>Biarritz</option>
                          <option value="2">Paris Orly</option>
                          <option value="3">Paris CDG</option>
                          <option value="4">Bordeaux</option>
                          <option value="5">Toulouse</option>
                          <option value="6">Malaga</option>
                          <option value="7">Séoul</option>
                          <option value="8">Tokyo</option>
                          <option value="9">Rio</option>
                        </select>
                      </div>
                      <div class="pick-date bookinput-item">
                        <input <?php if (!$peuxChoisir) echo ('id="startDate2"'); ?> placeholder="Date de début" name="startDate2" <?php if ($peuxChoisir) echo 'disabled'; ?> <?php if (!empty($_GET['startDate2'])) echo ('value = ' . $_GET['startDate2']); ?> />
                      </div>
                      <div class="retern-date bookinput-item">
                        <input <?php if (!$peuxChoisir) echo ('id="endDate2"'); ?> placeholder="Date de fin" name="endDate2" <?php if ($peuxChoisir) echo 'disabled'; ?> <?php if (!empty($_GET['endDate2'])) echo ('value = ' . $_GET['endDate2']); ?> />
                      </div>
                      <div class="car-choose bookinput-item">
                        <select class="custom-select" name="Voiture" id="selectVoiture" placeholder="Marque">
                          <option selected value="1">Peugeot</option>
                          <option value="2">Renault</option>
                          <option value="3">Nissan</option>
                          <option value="4">Audi</option>
                          <option value="5">Alfa Romeo</option>
                          <option value="6">Tesla</option>
                        </select>
                      </div>
                      <div class="car-choose bookinput-item">
                        <input type="text" placeholder="Modèle" name="modeleVoiture" style="font-size : 16px;" maxlength="10" />
                      </div>
                      <div class="bookcar-btn bookinput-item" style="margin-left : 20px">
                        <button type="submit" name="submit" value="upload">Déposer</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } else {
  echo "Vous devez être connecté pour accéder à cette page";
} ?>
  <?php
  include 'footer.php';
  ?>
  </div>
  <?php
  include 'dependances.php';
  include 'dependancesRent.php';
  ?>
</body>
</html>