<?php
session_start();
if (!empty($_POST['indexSite'])) {
  $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
  $idSite = $_POST['indexSite'];
  $requete2 = "UPDATE sites SET disponible = 0 WHERE id LIKE $idSite";
  $requete_preparee2 = $bdd->prepare($requete2);
  $requete_preparee2->execute();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Site</title>
  <?php include 'classicHead.php' ?>
</head>
<body>
  <?php
  $bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
  $aeroportsNoms = array('Biarritz', 'Orly', 'Charles De Gaulle', 'Bordeaux', 'Toulouse', 'Malaga', 'Séoul', 'Tokyo', 'Rio de Janeiro');
  $tableau = array();
  $requete1 = "SELECT * FROM sites";
  $requete_preparee1 = $bdd->prepare($requete1);
  $requete_preparee1->execute();
  if (!$requete_preparee1->rowCount() == 0) {
    while ($results = $requete_preparee1->fetch()) {
      array_push($tableau, $results);
    }
  } else {
    //echo 'Nothing found';
  };
  include 'header.php';
  ?>
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Louer un site</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <br>
  <div class="container">
    <?php if (!empty($_POST['indexSite'])) {
      echo '<br><br><p Style = "font-size : 25px; text-align : center;">Le site a bien été acquis</p><br><br><br>';
    } ?>
    <h4>Listes des sites</h4>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>
            #
          </th>
          <th>
            Aéroport
          </th>
          <th>
            Lieu
          </th>
          <th>
            Parking
          </th>
          <th>
            Adresse
          </th>
          <th>
            Nombre de places
          </th>
          <th>
            Tarif (Place/Jour)
          </th>
          <th>
            Acquis
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($tableau as $ligne) {
          echo '
                    <tr>
                     <td>' . $ligne[0] . '</td>
                     <td>' . $aeroportsNoms[$ligne[1] - 1] . '</td>
                     <td>' . $ligne[2] . '</td>
                     <td>' . $ligne[3] . '</td>
                     <td>' . $ligne[4] . '</td>
                     <td>' . $ligne[5] . '</td>
                     <td>' . $ligne[7] . ' €</td>';
          if ($ligne[8] == 1) {
            echo '<td> 
                     <form action = "" method = "POST">
                      <input type = "hidden" name = "indexSite" value =' . $ligne[0] . ' />
                      <button type ="submit"  class="btn btn-large btn-theme btn-rounded"/>  Louer  </button></td></form>';
          } else {
            echo ' <td> OUI </td> </tr>
                       ';
          }
        }
        ?>
      </tbody>
    </table>
  </div>
  </div>
  <br>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>