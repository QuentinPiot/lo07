<?php
session_start();
require_once('date.php');
require_once('requete.php');
$dateD = $_SESSION["startDate2"];
$dateF = $_SESSION["endDate2"];
$choixA =  $_SESSION["Aeroport"];
$aeroport = "Biarritz";
$voiture = $_SESSION["Voiture"];
$modele = $_SESSION["Modele"];
$indexV = $_SESSION['idMarques'];
$siteV = $_SESSION['sitesVehicules'];
$Requete = new Requeteobjet( new PDO('mysql:host=localhost;dbname=Alhambra', 'root', ''));
$dateDA = new ObjetDate($dateD);
$dateDB = new ObjetDate($dateF);
$datetime1 = $dateDA->changerFormat();
$datetime2 = $dateDB->changerFormat();
$interval = $datetime1->diff($datetime2);
$nombreJours = $interval->format('%a');
switch ($choixA) {
  case  1:
    $aeroport = "Biarritz";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5791.021966553301!2d-1.530200450974003!3d43.47080553212326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd5115567ffe066d%3A0x1a5a224a9c9f5bd6!2sA%C3%A9roport+Biarritz+Pays+Basque!5e0!3m2!1sfr!2sfr!4v1555158262299!5m2!1sfr!2sfr";
    break;
  case 2:
    $aeroport = "Orly";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21055.08085053934!2d2.366138744838969!3d48.72678700551095!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e675b1fa6a3b1d%3A0x9d78ded743db8422!2sA%C3%A9roport+de+Paris-Orly!5e0!3m2!1sfr!2sfr!4v1555158412085!5m2!1sfr!2sfr";
    break;
  case 3:
    $aeroport = "Charles de Gaulle";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d83750.05547800746!2d2.576965775623852!3d49.006984741216804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e63e038e4ccf5b%3A0x42be0982f5ba62c!2sParis-Charles+De+Gaulle!5e0!3m2!1sfr!2sfr!4v1555158360492!5m2!1sfr!2sfr";
    break;
  case 4:
    $aeroport = "Bordeaux";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22606.544619615714!2d-0.7128232573348722!3d44.82356796982325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd54daf55f7d4389%3A0x610c52b21795ac26!2sA%C3%A9roport+de+Bordeaux!5e0!3m2!1sfr!2sfr!4v1555158479005!5m2!1sfr!2sfr";
    break;
  case 5:
    $aeroport = "Toulouse";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22939.129033375415!2d1.3694411393272428!3d43.62383288934331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aea552ded709b5%3A0x66f417d01d8268f6!2sA%C3%A9roport+Toulouse-Blagnac!5e0!3m2!1sfr!2sfr!4v1555158602716!5m2!1sfr!2sfr";
    break;
  case 6:
    $aeroport = "Malaga";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12799.556157205601!2d-4.492470841433956!3d36.67716884750976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd72fa1876ba30a5%3A0x57b5ad2f47de3e9a!2sA%C3%A9roport+de+Malaga-Costa+del+Sol!5e0!3m2!1sfr!2sfr!4v1555158646096!5m2!1sfr!2sfr";
    break;
  case 7:
    $aeroport = "Séoul";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60983.03783620152!2d126.46169608415526!3d37.46807348044136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357b9a833a5efa59%3A0x8d4ba096cb5cbed4!2sIncheon+International+Airport!5e0!3m2!1sfr!2sfr!4v1555158712431!5m2!1sfr!2sfr";
    break;
  case 8:
    $aeroport = "Tokyo";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25933.493139611528!2d140.37649195259513!3d35.77515904880653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6022f379d1bd3757%3A0xd56e29a162771aa1!2sA%C3%A9roport+international+de+Narita!5e0!3m2!1sfr!2sfr!4v1555158799788!5m2!1sfr!2sfr";
    break;
  case 9:
    $aeroport = "Rio de Janeiro";
    $carte = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29421.6027229472!2d-43.240109760901085!3d-22.813564941136665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99799368903d71%3A0xead21443a9686abf!2sGale%C3%A3o%2C+Rio+de+Janeiro+-+%C3%89tat+de+Rio+de+Janeiro%2C+Br%C3%A9sil!5e0!3m2!1sfr!2sfr!4v1555158857664!5m2!1sfr!2sfr";
    break;
}
switch ($voiture) {
  case "Peugeot":
    $indice = 1.3;
    break;
  case "Renault":
    $indice = 1;
    break;
  case "Nissan":
    $indice = 1.2;
    break;
  case "Audi":
    $indice = 2;
    break;
  case "Alfa Romeo":
    $indice = 3;
    break;
  case "Tesla":
    $indice = 5;
    break;
}
$bdd = new PDO('mysql:host=localhost;dbname=Alhambra', 'root', '');
$requeteNomSite = "SELECT * FROM sites WHERE id LIKE $siteV";
//echo $requete1;
$requete_NomSite = $bdd->prepare($requeteNomSite);
$requete_NomSite->execute();
$adresse = "";
if (!$requete_NomSite->rowCount() == 0) {
  while ($resultsSite = $requete_NomSite->fetch()) {
    $nomSite = "Parking " . $resultsSite[3];
    $adresse = $resultsSite[4];
  }
} else {
  // echo 'Nothing found';
};
$erreur = false;
if (isset($_POST['idClient'])) {
  $prixReservation = ($indice * $nombreJours * 25);
  $nouvelArgent = $_SESSION['argent'] - $prixReservation;

  if($_SESSION['argent'] >= $prixReservation){
  $idC = $_POST['idClient'];
  $dateD = $_POST['dateD'];
  $dateF = $_POST['dateF'];
  $choixA =  $_POST['aeroport'];
  $cV = $_POST['voiture'];
  $sV = $_POST['site'];
  $valeurs1 = [
    'c' => $idC,
    'v' => $cV,
    'dd' => $dateD,
    'df' => $dateF,
    'a' => $choixA,
    's' => $sV,
    'p' => $prixReservation
  ];

  $Requete->requeteSimple('INSERT INTO reservation (client, voitureID, dateDebut, dateFin, aeroport,site, prix)
  VALUES (:c, :v,:dd, :df,:a,:s,:p)',$valeurs1);

  $requete3 = "SELECT reservation FROM voiture WHERE id LIKE $cV";
  //echo $requete1;
  $requete_preparee3 = $bdd->prepare($requete3);
  $requete_preparee3->execute();
  if (!$requete_preparee3->rowCount() == 0) {
    while ($results3 = $requete_preparee3->fetch()) {
      $reservation = $results3[0];
    }
  } else {
    // echo 'Nothing found';
  };

  if ($reservation == "{}") {
    $nouvelleReservation = json_encode(array(array($dateD, $dateF, $idC)));
    $valeurs2 = [
      'res' => $reservation,
      'idVoiture' => $cV
    ];

    $Requete->requeteSimple("UPDATE voiture SET reservation = '$nouvelleReservation' WHERE id= $cV",null);
   
  } else {
    $nouvelleReservation = json_decode($reservation);
    array_push($nouvelleReservation, array($dateD, $dateF, $idC));
    $nouvelleReservation = json_encode($nouvelleReservation);
    $requete2 = "UPDATE voiture SET reservation = '$nouvelleReservation' WHERE id= $cV";
    $requete_preparee2 = $bdd->prepare($requete2);
    $requete_preparee2->execute();
    
  }

  $Requete->requeteSimple("UPDATE membres SET argent = '$nouvelArgent' WHERE id= $idC",null);
  $_SESSION['argent'] = $nouvelArgent;
   header("Location: index.php");
}else {
  $erreur = true;
}
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Réservation</title>
  <?php include 'classicHead.php' ?>
  <script>
    function submitForm() {
      setTimeout("document.getElementById('formSuccess').submit()", 3000);
    }
  </script>
</head>
<body>
  <?php
  include 'header.php';
  ?>
  <!-- end header -->
  <section id="inner-headline">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="inner-heading">
            <h2>Réservation</h2>
          </div>
        </div>
        <div class="span8">
          <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li class="active">Réservation</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <iframe src="<?php echo $carte; ?>" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="container">
      <div class="row">
        <div class="span8">
          <article>
            <div class="top-wrapper">
              <div class="post-heading">
                <br> <br> <br>
                <h4><a href="#">Réservation Aéroport de <?php echo $aeroport ?></a></h4>
              </div>
              <!-- start flexslider -->
              <div class="flexslider">
                <ul class="slides">
                  <li>
                    <img src="public/img/Vehicules/<?php echo $voiture; ?>.png" style="width : 500px;" alt="" />
                  </li>
                </ul>
              </div>
              <!-- end flexslider -->
            </div>
            <form action="" method="POST" id="formSuccess">
              <input type="hidden" name="idClient" value=<?php echo ($_SESSION['id']); ?>>
              <input type="hidden" name="dateD" value=<?php echo ($dateD); ?>>
              <input type="hidden" name="dateF" value=<?php echo ($dateF); ?>>
              <input type="hidden" name="aeroport" value=<?php echo ($choixA); ?>>
              <input type="hidden" name="voiture" value=<?php echo ($indexV); ?>>
              <input type="hidden" name="site" value=<?php echo ($siteV); ?>>
              <div class="cta floatright">
                <!--
 <button type ="submit" name ="submit" class="btn btn-large btn-theme btn-rounded" href="#Validation" id = "connectClick" data-toggle="modal" onClick="setTimeout(function(){
            window.location.href = 'index.php';
         }, 3000);"
                -->
                <button type="button" id="connectClick" class="btn btn-large btn-theme btn-rounded" href="#Validation" data-toggle="modal" onClick="submitForm();"> Réserver le véhicule</button>
              </div>
            </form>
            <div id="Validation" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="validationLabel" aria-hidden="true">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <?php if($_SESSION['argent'] >= ($indice * $nombreJours * 25)) echo '<h4 id="validationLabel">La demande a été prise en compte</h4>';
                  else echo "<h4 id='validationLabel'>Vous n'avez pas assez d'argent pour réserver</h4>";
                  ?>
                
              </div>
              <div class="modal-body">
                <p> Redirection dans quelques instants ...
              </div>
            </div>
            <p>
              <h4>Prix : <?php echo ($indice * $nombreJours * 25); ?> €</h4>
            </p>
          </article>
        </div>
        <div class="span4">
          <aside class="right-sidebar">
            <div class="widget">
              <h5 class="widgetheading">Informations</h5>
              <ul class="folio-detail">
                <li><label>Aéroport :</label> <?php echo " $aeroport "; ?> </li>
                <li><label>Site :</label> <?php echo " $nomSite "; ?> </li>
                <li><label>Adresse :</label> <?php echo " $adresse "; ?> </li>
                <li><label>Date de départ :</label> <?php echo " $dateD "; ?></li>
                <li><label>Date d'arrivée :</label> <?php echo " $dateF "; ?></li>
                <li><label>Nombre de jours :</label> <?php echo " $nombreJours jours"; ?></li>
                <li><label>Voiture :</label> <?php echo " $voiture "; ?></li>
                <li><label>Modèle :</label> <?php echo " $modele "; ?></li>
              </ul>
            </div>
            <div class="widget">
              <h5 class="widgetheading">Détails</h5>
              <p>
                Le plein doit être effectué avant la remise du véhicule.
                L'assurance est comprise dans le prix.<br>
                Le tarif pour un véhicule <?php echo "$voiture"; ?> est de <?php echo (25 * $indice . "€ par jour"); ?>
              </p>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <?php
  include 'dependances.php';
  ?>
</body>
</html>